package com.adeli;

import java.util.ArrayList;
import java.util.List;

class SummerThread implements Runnable {
    public static List<Long> subTotal = new ArrayList<>();

    private long start;
    private long end;
    private long sum;

    public SummerThread(Long start, Long end) {
        this.start = start;
        this.end = end;
        this.sum = 0L;
    }

    public void run() {
        System.out.println(Thread.currentThread().getName());
        for (long i = start; i < end; i++) {
            sum = sum + i;
        }
        subTotal.add(sum);
    }
}
