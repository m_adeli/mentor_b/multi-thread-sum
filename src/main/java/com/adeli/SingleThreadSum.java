package com.adeli;

import java.sql.Timestamp;

public class SingleThreadSum {
    static long sum = 0L;

    public static void main(String[] args) {

        Thread t = new Thread(() -> {
            System.out.println(Thread.currentThread().getName());
            for (long i = 1L; i < 1_000_000_000L; i++) {
                sum = sum + i;
            }
            for (long i = 1L; i < 1_000_000_000L; i++) {
                sum = sum + i;
            }
            for (long i = 1L; i < 1_000_000_000L; i++) {
                sum = sum + i;
            }
            for (long i = 1L; i < 1_000_000_000L; i++) {
                sum = sum + i;
            }
        });

        Timestamp sTime = new Timestamp(System.currentTimeMillis());
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Timestamp eTime = new Timestamp(System.currentTimeMillis());
        long duration = eTime.getTime() - sTime.getTime();
        System.out.println("Total is: " + sum);
        System.out.println("Duration is: " + duration);

    }
}
