package com.adeli;

import java.sql.Timestamp;

public class MultiThreadSum {

    public static void main(String[] args) {

        Thread t1 = new Thread(new SummerThread(0L, 1_000_000_000L));
        Thread t2 = new Thread(new SummerThread(0L, 1_000_000_000L));
        Thread t3 = new Thread(new SummerThread(0L, 1_000_000_000L));
        Thread t4 = new Thread(new SummerThread(0L, 1_000_000_000L));

        Timestamp sTime = new Timestamp(System.currentTimeMillis());
        t1.start();
        t2.start();
        t3.start();
        t4.start();

        try {
            t1.join();
            t2.join();
            t3.join();
            t4.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        long total = SummerThread.subTotal.stream().mapToLong(x -> (long) x).sum();

        Timestamp eTime = new Timestamp(System.currentTimeMillis());
        long duration = eTime.getTime() - sTime.getTime();

        System.out.println("Total is: " + total);
        System.out.println("Duration is: " + duration);
    }
}